''' Copyright (c) 2024 openEuler Community
 EasySoftware is licensed under the Mulan PSL v2.
 You can use this software according to the terms and conditions of the Mulan PSL v2.
 You may obtain a copy of Mulan PSL v2 at:
     http://license.coscl.org.cn/MulanPSL2
 THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 See the Mulan PSL v2 for more details.
'''

import yaml
import traceback
import argparse
import sys


def check_yaml(path):
    try:
        data = yaml.load_all(open(path)).__next__()['users']
        print('load yaml success')
    except:
        print('load yaml failed')
        print(traceback.format_exc())
        return False
    dl = []
    for i in data:
        dl.append(i['gitee_id'])
    if len(dl) != len(set(dl)):
        print('failed: gitee id already existed')
        return False
    else:
        print('success: input contributor info')
        return True
    return True


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='yaml file path.')
    parser.add_argument('--path', '-p', help='-yaml file path')
    args = parser.parse_args()
    check_ok = check_yaml(args.path)
    if check_ok is False:
        sys.exit(1)
